import React, { Component } from 'react';
class Header extends Component {
  render() {
    return (
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="/">Restaurant App</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/bill-calculator">Bill Calculator</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/promotion-code">Promotion Code</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/reservation-seat">Reservation Seat</a>
            </li>
        
            </ul>
      </nav>
    );
  }
}

export default Header;
