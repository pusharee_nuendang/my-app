import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import App from './App'
import Bill_calculator from './components/BillCalculator'
import Promotion_code from './components/PromotionCode'
import Promotion_code_add from './components/PromotionCodeAdd'
import Promotion_code_edit from './components/PromotionCodeEdit'


import Reservation_seat from './components/ReservationSeat'
import Reservation_seat_add from './components/ReservationSeatAdd'


const routing = (
    <Router>
      <div>
        <Route path="/" component={App} />
        <Route path="/bill-calculator" component={Bill_calculator} />
        <Route path="/promotion-code-add" component={Promotion_code_add} />
        <Route path="/promotion-code-edit/:id" component={Promotion_code_edit} />
        <Route path="/promotion-code" component={Promotion_code} />
        <Route path="/reservation-seat" component={Reservation_seat} />
        <Route path="/reservation-seat-add" component={Reservation_seat_add} />


      </div>
    </Router>
  )
  ReactDOM.render(routing, document.getElementById('root'))