import React, { Component } from 'react';

class Bill_calculator extends Component {
  render() {
    return (
      <div class="container mt-5">
      <div class="card">
        <div class="card-body">
        <h5 class="card-title">Bill Calculator</h5>
        <form>
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-2 col-form-label">Buffet per person</label>
              <div class="col-sm-10">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                  </div>
                  <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)"  value="459" type="number"/>
                  <div class="input-group-append">
                    <span class="input-group-text">.00</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-2 col-form-label">Seat </label>
              <div class="col-sm-10">
                <input type="text"  class="form-control" id="staticEmail" value=""  type="number"/>
              </div>
            </div>
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-2 col-form-label">Promotion code</label>
              <div class="col-sm-10">
                <input type="text" readonly class="form-control" id="staticEmail" value=""  type="text"/>
              </div>
            </div>
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-2 col-form-label"></label>
              <div class="col-sm-10">
                  <button type="button" class="btn btn-light">Reset</button>
                  <button type="button" class="btn btn-dark ml-3">Calculator</button>
              </div>
            </div>
           

          </form>
      </div>
      </div>
      </div>
    );
  }
}

export default Bill_calculator;
