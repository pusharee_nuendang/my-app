import React, { Component } from 'react';
class ReservationSeatAdd extends Component {
    render() {
        return (
            <div class="container mt-5">
                <div class="card">
                    <div class="card-header">
                        Reservation Seat
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Date </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="staticEmail" value="" type="number" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Person </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="staticEmail" value="" type="number" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Zone</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" id="inputGroupSelect01">
                                        <option selected>Choose...</option>
                                        <option value="1">Zone A</option>
                                        <option value="2">Zone B</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Firstname </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="staticEmail" value="" type="number" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Lastname </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="staticEmail" value="" type="number" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Tel. </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="staticEmail" value="" type="number" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="button" class="btn btn-light">Reset</button>
                                    <button type="button" class="btn btn-dark ml-3">Reservation</button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ReservationSeatAdd;
