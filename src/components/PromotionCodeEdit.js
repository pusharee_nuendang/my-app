import React, { Component } from 'react';
class PromotionCodeEdit extends Component {
  render() {
    return (
        <div class="container mt-5">
        <div class="card">
        <div class="card-header">
              EDIT FORM 
            </div>
          <div class="card-body">
            <form>
              <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">CODE </label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="staticEmail" value="" type="text" />
                </div>
              </div>
              <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Discount Type </label>
                <div class="col-sm-10">
                  <select class="custom-select" id="inputGroupSelect01">
                    <option selected>Choose...</option>
                    <option value="1">Percentage</option>
                    <option value="2">Buy X Get Y For Free</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Percentage</label>
                <div class="col-sm-4">
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" />
                    <div class="input-group-append">
                      <span class="input-group-text"> % </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Conditions</label>
                <div class="col-sm-10">
                  <select class="custom-select" id="inputGroupSelect01">
                    <option selected>Choose...</option>
                    <option value="1">Price</option>
                    <option value="2">Person</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Over price</label>
                <div class="col-sm-4">
                  <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">$</span>
                      </div>
                      <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" />
                      <div class="input-group-append">
                        <span class="input-group-text">.00</span>
                      </div>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Person</label>
                <div class="col-sm-4">
                  <input type="text" readonly class="form-control" id="staticEmail" value="" type="number" />
                </div>
              </div>
 
              <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Buy</label>
                <div class="col-sm-4">
                  <input type="text" readonly class="form-control" id="staticEmail" value="" type="number" />
                </div>
              </div>
              <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Free</label>
                <div class="col-sm-4">
                  <input type="text" readonly class="form-control" id="staticEmail" value="" type="number" />
                </div>
              </div>
              <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-10">
                  <button type="button" class="btn btn-light">Reset</button>
                  <button type="button" class="btn btn-dark ml-3">SAVE</button>
                </div>
              </div>


            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default PromotionCodeEdit;
