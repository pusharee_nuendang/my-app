import React from 'react'
import PropTypes from 'prop-types'
import Promotion from './Promotion'

const PromotionList = ({ todos, onTodoClick }) => (
  <ul>
    {todos.map((todo, index) => (
      <Promotion key={index} {...todo} onClick={() => onTodoClick(index)} />
    ))}
  </ul>
)

PromotionList.propTypes = {
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      completed: PropTypes.bool.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  onTodoClick: PropTypes.func.isRequired
}

export default PromotionList