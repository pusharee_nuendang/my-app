import React, { Component } from 'react';
class PromotionCode extends Component {
  render() {
    return (
      <div class="container mt-5">
      <div class="card">
        <div class="card-body">
        <h5 class="card-title">Promotion Code</h5>
        <a href="/promotion-code-add"><button type="button" class="btn btn-sm btn-info mt-3 mb-3 " > <i class="fas fa-plus"></i> ADD</button></a>
        <table class="table table-hover">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Code</th>
              <th scope="col">Types of Discount</th>
              <th scope="col">
              
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
              <td>
                <a href="/promotion-code-edit/1"><button type="button" class="btn btn-success btn-sm"><i class="fas fa-edit"></i></button></a>
                <button type="button" class="btn btn-danger btn-sm ml-2"><i class="fas fa-times"></i></button>
              </td>
            </tr>
           
          </tbody>
        </table>
        </div>
      </div>
      </div>
    );
  }
}

export default PromotionCode;
