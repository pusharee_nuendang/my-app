import React, { Component } from 'react';
import './App.css';
import Header from './Header'
class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <div class="container mt-5">
          <div class="card">
          <div class="card-body">
              <i class="fas fa-utensils"></i> Restaurant Bill Calculator
          </div>
        </div>
        </div>
      </div>
    );
  }
}

export default App;
